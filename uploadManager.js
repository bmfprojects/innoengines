// config the uploader
var options = {
    tmpDir:  __dirname + '/public/uploaded/tmp',
    publicDir: __dirname + '/public/uploaded',
    uploadDir: __dirname + '/public/uploaded/files',
    uploadUrl:  '/uploaded/files/',
    maxPostSize: 11000000000, // 11 GB
    minFileSize:  1,
    maxFileSize:  10000000000, // 10 GB
    acceptFileTypes:  /.+/i,
    // Files not matched by this regular expression force a download dialog,
    // to prevent executing any scripts in the context of the service domain:
    inlineFileTypes:  /\.(gif|jpe?g|png)$/i,
    imageTypes:  /\.(gif|jpe?g|png)$/i,
    imageVersions: {
        width:  80,
        height: 80
    },
    accessControl: {
        allowOrigin: '*',
        allowMethods: 'OPTIONS, HEAD, GET, POST, PUT, DELETE',
        allowHeaders: 'Content-Type, Content-Range, Content-Disposition'
    },
    nodeStatic: {
        cache:  3600 // seconds to cache served files
    }
};

var uploader        = require('blueimp-file-upload-expressjs')(options);
var express         = require('express'),
    router          = express.Router(),
    cors            = require('cors'),
    bodyparser      = require('body-parser'),
    session         = require('express-session'),
    methodOverride  = require('method-override');

router
.use(cors())
.use(session({secret  : 'secretimage'}))
.use(bodyparser.json({ limit: '1000mb'}))
.use(bodyparser.urlencoded({ limit: '1000mb'}))
.use(methodOverride())
.route('/upload')
.get(function(req, res) {
      uploader.get(req, res, function (obj) {
            res.json(obj); 
            console.log(obj.name);
      });
      
});
router
.use(cors())
.use(bodyparser.json({ limit: '1000mb'}))
.use(bodyparser.urlencoded({ limit: '1000mb'}))
.use(methodOverride())
.route('/upload')
.post(function(req, res) {

      uploader.post(req, res, function (obj) {
            res.json(obj); 
            console.log(obj);

      });
      
    });
router
.use(cors())
.use(bodyparser.json({ limit: '1000mb'}))
.use(bodyparser.urlencoded({ limit: '1000mb'}))
.use(methodOverride())
.route('/uploaded/files/:name')
.delete(function(req, res) {
    var name = req.params.name;
      uploader.delete(req, res, function (name) {
            res.json(name); 
            console.log(name);
      });
});

module.exports = router;