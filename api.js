var express 		= require('express'),
	config 			= require("./config"),
	bodyparser  	= require('body-parser'),
	mongodb 		= require('mongoskin'),
	cors 			= require('cors'),
	session 		= require('express-session'),
	fs 				= require('fs'),
	path 			= require('path'),
	http 			= require('http'),
	formidable 		= require('formidable'),
	qt 				= require('quickthumb'),
	multipart   	= require('connect-multiparty'),
	util			= require('util'),
	fs				= require('fs-extra'),
    uuid            = require('node-uuid'),
	db 				= mongodb.db(config.mongodb.inno1, {native_parser:true}); 
	router 			= express.Router(),
	methodOverride 	= require('method-override');

router.get('/formavatar/:name', function(req, res) {

  res.render('formtest', { title: 'Node Express', imagewonder: req.params.name }); 
  
});	
//=========================================================================
//IMAGES COLLECTION
//=========================================================================


router
    .use(cors())
    .use(bodyparser.json({ limit: '1000mb'}))
    .use(bodyparser.urlencoded({ limit: '1000mb'}))
    .use(methodOverride())
    .route('/getimage')
    .get(function (req, res) {
        db.collection('imageinfo').find({ account_id: req.user_id }).toArray(function (err, data){
            res.json(data);
            console.log(data);
        });
});


//=========================================================================
//BRICK COLLECTION
//=========================================================================


router
    .use(cors())
    .use(bodyparser.json({ limit: '1000mb'}))
    .use(bodyparser.urlencoded({ limit: '1000mb'}))
    .use(methodOverride())
    .route('/getbricklist')
    .get(function (req, res) {
        db.collection('brick').find({company_id : req.accountId}).toArray(function (err, data){
            res.json(data);
        });
    })

   .post(function (req, res) {

    var brick = req.body;
    brick.account_id = req.user_id;
    brick.company_id = req.accountId;
    brick.order      = 0;                                
    brick.createdBy  = req.byusername;   
    console.log(brick.company_id);
    db.collection('brick').insert(brick, function (err, data){
            res.json(data);
        });
    });

router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '1000mb'}))
    .use(bodyparser.json({limit: '1000mb'}))
    .use(methodOverride())
    .route('/getbricklist/:id')
    .get(function (req, res){
        db.collection('brick').findById(req.params.id, function (err, data){
            res.json(data);
        });
    })
    .put(function (req, res){
        var id = req.params.id;
        var brick = req.body;
        delete brick._id;

        brick.account_id = req.user_id;
        brick.company_id = req.accountId;
        brick.order      = 0;                                
        brick.createdBy  = req.byusername;

        db.collection('brick').updateById(req.params.id, brick, function (err, data){
            res.json(data);
        });
    })
    .delete(function (req, res){
        db.collection('brick').removeById(req.params.id, function(){
            res.json(null);
        });
    });

//=========================================================================
//FIELDS COLLECTION
//=========================================================================
router
    .use(cors())
    .use(bodyparser.json({ limit: '1000mb'}))
    .use(bodyparser.urlencoded({ limit: '1000mb'}))
    .use(methodOverride())
    .route('/fields/column1')
    .get(function (req, res) {
        db.collection('fields').find({column: 1}).toArray(function (err, data){
            res.json(data);
        });
    })
router
    .use(cors())
    .use(bodyparser.json({ limit: '1000mb'}))
    .use(bodyparser.urlencoded({ limit: '1000mb'}))
    .use(methodOverride())
    .route('/fields/column2')
    .get(function (req, res) {
        db.collection('fields').find({column: 2}).toArray(function (err, data){
            res.json(data);
        });
    })

router
    .use(cors())
    .use(bodyparser.json({ limit: '1000mb'}))
    .use(bodyparser.urlencoded({ limit: '1000mb'}))
    .use(methodOverride())
    .route('/fields/column3')
    .get(function (req, res) {
        db.collection('fields').find({column: 3}).toArray(function (err, data){
            res.json(data);
        });
    })
router
    .use(cors())
    .use(bodyparser.json({ limit: '1000mb'}))
    .use(bodyparser.urlencoded({ limit: '1000mb'}))
    .use(methodOverride())
    .route('/fields/column4')
    .get(function (req, res) {
        db.collection('fields').find({column: 4}).toArray(function (err, data){
            res.json(data);
        });
    })

router
    .use(cors())
    .use(bodyparser.json({ limit: '1000mb'}))
    .use(bodyparser.urlencoded({ limit: '1000mb'}))
    .use(methodOverride())
    .route('/fields')
    .get(function (req, res) {
        db.collection('fields').find({company_id : req.accountId}).toArray(function (err, data){
            res.json(data);
        });
    })

   .post(function (req, res) {

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();


    var fields = req.body;
    fields.account_id = req.user_id;
    fields.company_id = req.accountId;
    fields.order      = 0;                                
    fields.createdBy  = req.byusername;
    fields.dateCreate = today;

    db.collection('fields').insert(fields, function (err, data){
            res.json(data);
        });
    });

router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '1000mb'}))
    .use(bodyparser.json({limit: '1000mb'}))
    .use(methodOverride())
    .route('/fields/:id')
    .get(function (req, res){
        db.collection('fields').findById(req.params.id, function (err, data){
            res.json(data);
        });
    })
    .put(function (req, res){
        var id = req.params.id;
        var fields = req.body;
        delete fields._id;

        fields.account_id = req.user_id;
        fields.company_id = req.accountId;
        fields.order      = 0;                                
        fields.createdBy  = req.byusername;

        db.collection('fields').updateById(req.params.id, fields, function (err, data){
            res.json(data);
        });
    })
    .delete(function (req, res){
        db.collection('fields').removeById(req.params.id, function(){
            res.json(null);

        });
    });

//=========================================================================
//INNOVATION COLLECTION
//=========================================================================

router
    .use(cors())
    .use(bodyparser.json({ limit: '1000mb'}))
    .use(bodyparser.urlencoded({ limit: '1000mb'}))
    .use(methodOverride())
    .route('/imagessave')
    .get(function (req, res){
        db.collection('imagessave').find({ account_id: req.user_id }).toArray(function (err, data){
            res.json(data);
        });
    })

   .post(function (req, res) {

    var imagessave = req.body;
    imagessave.account_id = req.user_id;
    db.collection('imagessave').insert(imagessave, function (err, data){
            res.json(data);
        });
    });

router
	.use(cors())
	.use(bodyparser.json({ limit: '1000mb'}))
	.use(bodyparser.urlencoded({ limit: '1000mb'}))
	.use(methodOverride())
	.route('/newinnovation')
	.get(function (req, res){
		db.collection('innovations').find({ account_id: req.user_id }).toArray(function (err, data){
			res.json(data);
		});
	})

   .post(function (req, res) {

    var inno_id = uuid.v4();
    var today = new Date();

	var innovation          = req.body;
    innovation.inno_id      = inno_id;
	innovation.account_id   = req.user_id;
    innovation.createdby    = req.byusername;
    innovation.dateCreate   = today;

    db.collection('innovations').insert(innovation, function (err, data){
	        res.json(data);
		});
	});
router
    .use(cors())
    .use(bodyparser.json({ limit: '1000mb'}))
    .use(bodyparser.urlencoded({ limit: '1000mb'}))
    .use(methodOverride())
    .route('/saveinno/:innoid')
    .post(function (req, res) {

    var innovation = req.body;
    innovation.inno_id = req.params.innoid;

    db.collection('values').insert(innovation, function (err, data){
            res.json(data);
        });
    });


router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '1000mb'}))
    .use(bodyparser.json({limit: '1000mb'}))
    .use(methodOverride())
    .route('/newinnovation/get_inno/:inno_id')
    .get(function (req, res){
        db.collection('innovations').find({inno_id : req.params.inno_id}).toArray(function (err, data){
            res.json(data);
        });
    });

router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '1000mb'}))
    .use(bodyparser.json({limit: '1000mb'}))
    .use(methodOverride())
	.route('/newinnovation/:id')
	.get(function (req, res){
		db.collection('innovations').findById(req.params.id, function (err, data){
			res.json(data);
		});
	})

	.put(function (req, res){
		var id = req.params.id;
        var image = {};
		var innovation = req.body;
		delete innovation._id;
		  
        innovation.account_id = req.user_id;
	    innovation.image = req.body.ideaproducts;

		db.collection('innovations').updateById(req.params.id, innovation, function (err, data){
			res.json(data);
		});
	})
	.delete(function (req, res){
		db.collection('innovations').removeById(req.params.id, function(){
			res.json(null);
		});
	});


router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '1000mb'}))
    .use(bodyparser.json({limit: '1000mb'}))

.post('/uploading', function (req, res, next) {

    var form = new formidable.IncomingForm();
    form.parse(req, function(err, fields, files) {
        // `file` is the name of the <input> field of type `file`
        var old_path = files.file.path,
        	avatar = files.file.name,
            file_size = files.file.size,
            file_ext = files.file.name.split('.').pop(),
            index = old_path.lastIndexOf('/') + 1,
            file_name = old_path.substr(index),
            // new_path = path.join(process.env.PWD, '/uploads/', file_name + '.' + file_ext);
            new_path = path.join(process.env.PWD, '/uploads/', avatar);

        fs.readFile(old_path, function(err, data) {
            fs.writeFile(new_path, data, function(err) {
                fs.unlink(old_path, function(err) {
                    if (err) {
                        res.status(500);
                        res.json({'success': false});
                    } else {
                        res.status(200);
                        res.json({'success': true});
                    }
                });
            });
        });
    });
})

// Use quickthumb
.use(qt.static(__dirname + '/'))

.post('/imageupload', function (req, res, multipart){
  var form = new formidable.IncomingForm();
  var files = [];
  var fields = [];
  form.parse(req, function(err, fields, files) {
    
    res.writeHead(200, {'content-type': 'text/plain'});
    res.write('received upload:\n\n');
    res.end(util.inspect({fields: fields, files: files}));
  });

  form.on('end', function(fields, files) {
    /* Temporary location of our uploaded file */

    var temp_path = this.openedFiles[0].path;
    /* The file name of the uploaded file */
    var file_ext = this.openedFiles[0].name.split('.').pop();
    var index =  temp_path.lastIndexOf('/') + 1;
    // var file_name = temp_path.substr(index);

    var file_name = this.openedFiles[0].name;
    /* Location where we want to copy the uploaded file */
    var new_location = 'uploads/';
     // new_path = path.join(process.env.PWD, '/uploads/', file_name + '.' + file_ext);
    // fs.copy(temp_path, new_location + file_name + '.' + file_ext, function(err) {  
    fs.copy(temp_path, new_location + file_name, function(err) {  
    	var images = [];
        images = file_name;
        console.log(images);
      if (err) {
        console.error(err);
      } else {
        console.log("success!");
      }
    });
  });
});


module.exports = router;