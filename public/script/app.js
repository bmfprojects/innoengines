'use strict';

var app = angular.module('innoengines', 
	[

		'ngResource',
		'ngRoute', 
		'restangular',
		'ngAnimate',
		'mgcrea.ngStrap', 
		'ui.bootstrap', 
		'ngMessages',
		'ngTable',
		'ngUpload',
		'angularFileUpload',
		'dndLists'
		
	]);

// get the hostname and port from the server
// ==========================================
app.directive("dynamicName",function($compile){
    return {
        restrict:"A",
        terminal:true,
        priority:1000,
        link:function(scope,element,attrs){
            element.attr('name', scope.$eval(attrs.dynamicName));
            element.removeAttr("dynamic-name");
            $compile(element)(scope);
        }
    }
});

var hostname = window.location.hostname;
var port = window.location.port;
var api = hostname + ':' + port;

//===========================================
// app.constant('imageLocation','http://'+ api);
app.constant('DbCollection', 'http://'+ api);

app.config(function($routeProvider, $locationProvider, RestangularProvider, DbCollection){

$locationProvider.html5Mode(true);

	$routeProvider

	.when('/', {
		controller : 'homeCtrl',
		templateUrl: 'views/homepage.html'
	})
	.when('/formupload', {
		templateUrl: 'views/form.html'
	})
	.when('/getlistimage', {
		controller: 'imageCtrl',
		templateUrl: 'views/images.html'
	})
	.when('/imageroute', {
		controller: 'uploadCtrl',
		templateUrl: 'views/innovation/upload.html'
	})
	
	.when('/create', {
		controller: 'createInnovationCtrl',
		templateUrl: 'views/innovation/create_inno.html',
	})
	.when('/create/:inno_id', {
		controller: 'getBricksampleCtrl',
		templateUrl: 'views/innovation/getbrick_inno.html',
			resolve: {
        getBrick_id: function(Restangular, $http, $rootScope, $route){
              return $http.get(DbCollection + '/api/newinnovation/get_inno/'+ $route.current.params.inno_id);
          }
        }
	})
	.when('/test', {
		controller: 'getBricksampleCtrl',
		templateUrl: 'views/innovation/getbrick_inno.html',
		innovationdata: function(Restangular, $http, $rootScope, $route){
              return $http.get(DbCollection + '/api/newinnovation/get_inno/'+ $route.current.params.inno_id);
          }
	})
	.when('/editinno/:id', {
		controller: 'EditInnovationCtrl',
		templateUrl: 'views/innovation/getbrick_inno.html',
		resolve: {
	          innovation: function(Restangular, $http, $rootScope, $route){
	           // return Restangular.one(DbCollection + 'api/newinnovation', $route.current.params.id).get();
	              return $http.get(DbCollection + '/api/newinnovation/'+ $route.current.params.id);
			     //  .success(function(data){
			     //  $rootScope.account = data;
    				// });
	          }
	        }
	})
   .when('/viewinno/:id', {
		controller: 'EditInnovationCtrl',
		templateUrl: 'views/innovation/create_innosave.html',
		resolve: {
	          innovation: function(Restangular, $http, $rootScope, $route){
	              return $http.get(DbCollection + '/api/newinnovation/'+ $route.current.params.id);
	          }
	        }
	})
	.when('/create_presentation', {
		templateUrl: 'views/innovation/create_presentation.html'
	})
	.when('/presentation', {
		templateUrl: 'views/innovation/save_presentation.html'
	})
	.when('/view_inventory', {
		controller: 'createInnovationCtrl',
		templateUrl: 'views/innovation/view_inventory.html'
	})
	.when('/administrator', {
		controller: 'AdminCtrl',
		templateUrl: 'views/innovation/administrator.html'
	})
	.when('/administrator/:id', {
		controller: 'AdminCtrl',
		templateUrl: 'views/innovation/administrator.html',
		resolve: {
	      getbrick: function(Restangular, $http, $rootScope, $route){
	          return $http.get(DbCollection + '/api/getbricklist/'+ $route.current.params.id);
	      }
	    }
	})
	.when('/connect', {
		templateUrl: 'views/innovation/connect.html'
	})
	.when('/imageform', {
		templateUrl: 'views/formtest.html'
	})





	//for the administrator route

	.when('/accountlist', {
		templateUrl : 'create_account.html',
		controller : 'accountCtrl'
	})

	.when('/viewaccount/:id', {
		templateUrl: 'account.html',
		controller: 'AdduserCtrl',
		    resolve: {
	          Account: function(Restangular, $route){
	            return Restangular.one('accountname', $route.current.params.id).get();
	          }
	        }
	})

	  RestangularProvider.setBaseUrl(DbCollection);
      RestangularProvider.setRestangularFields({
        id: '_id'
      });
      
      RestangularProvider.setRequestInterceptor(function(elem, operation, what) {
        
        if (operation === 'put') {
          elem._id = undefined;
          return elem;
        }
        return elem;
      });

});
