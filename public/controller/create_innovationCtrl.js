'use strict';

var app = angular.module('innoengines');

app.controller('createInnovationCtrl', function($scope, $modal, $log, $alert, Innovation, $rootScope, $route, $routeParams, DbCollection, Restangular, $upload, $resource, $http, $q, $filter, ngTableParams, $location){





$scope.beginupload = function(){

       $http.post(DbCollection + '/uploader/upload/')
        .success(function(data){
            $scope.imagesresult = data;
            console.log(data);
         });
 
    };

  $scope.openmodal = function (size, id) {
 
    var modalInstance = $modal.open({
      templateUrl: 'myModalContent.html',
      controller: $scope.model,
      size: size,
      resolve: {
            innovation: function($http){
                return $http.get(DbCollection + '/api/newinnovation/'+ id);
            }
          }
    });

    modalInstance.result.then(function (selectedItem) {
      $scope.selected = selectedItem;
      }, function () {
        $log.info('Modal dismissed at: ' + new Date());
      });

    };

    $scope.model = function($scope, $modalInstance, $modal, innovation, $log, $alert, $rootScope, $route, $routeParams, DbCollection, Restangular, $upload, $resource, $http, $q, $filter, ngTableParams, $location){


        $rootScope.imagepath = DbCollection + '/uploaded/files/';

        $scope.deleteimage = function(index, name){

          $http.delete(DbCollection + '/uploader/uploaded/files/' + name).
          success(function(data){
            $scope.imagedetail.splice(index, 1)[0];
          });

        }

        $rootScope.imagehead = 'Image Actions';
        $scope.image = ['image url', 'image delete', 'image thumbnail'];
        $scope.imageresult = {};



          $http.get(DbCollection + '/uploader/upload/').
          success(function(data){
            $scope.uploaded = data.files;
          });


          $scope.onFileSelect = function($files) {

            console.log($files);

            //$files: an array of files selected, each file has name, size, and type.
              for (var i = 0; i < $files.length; i++) {

              $scope.file = $files[i];

                    $scope.imagedetail = [];
                    $scope.imagedetail = $files;

              $scope.upload = $upload.upload({
                url: DbCollection +'/uploader/upload', //upload.php script, node.js route, or servlet url
                method: 'POST', 
                //'POST' or 'PUT',
                //headers: {'header-key': 'header-value'},
                //withCredentials: true,
                data: {myObj: $scope.myModelObj},
                file: $scope.file, // or list of files ($files) for html5 only
                //fileName: 'doc.jpg' or ['1.jpg', '2.jpg', ...] // to modify the name of the file(s)
                // customize file formData name ('Content-Disposition'), server side file variable name. 
                fileFormDataName: $scope.file, //or a list of names for multiple files (html5). Default is 'file' 
                // customize how data is added to formData. See #40#issuecomment-28612000 for sample code
                //formDataAppender: function(formData, key, val){}
              }).progress(function(evt) {

                console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));

              }).success(function(data, status, headers, config) {
                // file is uploaded successfully
                
                  
                return $scope.imageresult = data.files;
                // console.log($scope.imageresult.files.length);

                // $scope.image = {};
                // $scope.image = $scope.file;

              });

              //.error(...)
              //.then(success, error, progress); 
              // access or attach event listeners to the underlying XMLHttpRequest.
              //.xhr(function(xhr){xhr.upload.addEventListener(...)})
            }
            /* alternative way of uploading, send the file binary with the file's content-type.
               Could be used to upload files to CouchDB, imgur, etc... html5 FileReader is needed. 
               It could also be used to monitor the progress of a normal http post/put request with large data*/
            // $scope.upload = $upload.http({...})  see 88#issuecomment-31366487 for sample code.
          };

            $scope.mytime = new Date();

            $scope.hstep = 1;
            $scope.mstep = 15;

            $scope.options = {
              hstep: [1, 2, 3],
              mstep: [1, 5, 10, 15, 25, 30]
            };

            $scope.ismeridian = true;

            $scope.toggleMode = function() {
              $scope.ismeridian = ! $scope.ismeridian;
            };
             $scope.update = function() {
                var d = new Date();
                d.setHours( 14 );
                d.setMinutes( 0 );
                $scope.mytime = d;
              };

              $scope.changed = function () {
                $log.log('Time changed to: ' + $scope.mytime);
                console.log($scope.mytime);
              };

              $scope.clear = function() {
                $scope.mytime = null;
              };

            $scope.innovation = innovation.data;
            console.log($scope.innovation);

            $scope.ok = function () {
              // $modalInstance.close($scope.selected.item);
              $modalInstance.dismiss('cancel');
            };

            $scope.cancel = function () {
              $modalInstance.dismiss('cancel');
            };

            // $scope.myInterval = 5000;

            // var slides = $scope.slides = [];

            // $scope.addSlide = function() {
            //   var newWidth = 600 + slides.length + 1;
            //   slides.push({
            //     image: 'http://placekitten.com/' + newWidth + '/300',
            //     text: ['More','Extra','Lots of','Surplus'][slides.length % 4] + ' ' +
            //       ['Cats', 'Kittys', 'Felines', 'Cutes'][slides.length % 4]
            //   });
            // };

            // for (var i=0; i<4; i++) {
            //   $scope.addSlide();
            // }

    }

    $http.get(DbCollection + '/api/newinnovation/')
    .then(function(result){

        $scope.inventory = result.data;
        $scope.sortingOrder = sortingOrder;
        $scope.reverse = true;
        $scope.filteredItems = [];
        $scope.groupedItems = [];
        $scope.itemsPerPage = 5;
        $scope.pagedItems = [];
        $scope.currentPage = 0;

        var searchMatch = function (haystack, needle) {
          

            if (!needle) {
                return true;
            }
            return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
         
        };

                // init the filtered items
                $scope.search = function () {
                    $scope.filteredItems = $filter('filter')($scope.inventory, function(item) {

                        for(var attr in item) {
                            
                            if( searchMatch(item[attr], $scope.query) )
                                return true;
                        }
                        return false;
                    });
                    // take care of the sorting order
                    if ($scope.sortingOrder !== '') {
                        $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                    }
                    $scope.currentPage = 0;
                    // now group by pages
                    $scope.groupToPages();
                };
                
                // calculate page in place
                $scope.groupToPages = function () {
                    $scope.pagedItems = [];
                    
                    for (var i = 0; i < $scope.filteredItems.length; i++) {
                        if (i % $scope.itemsPerPage === 0) {
                            $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [ $scope.filteredItems[i] ];
                        } else {
                            $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
                        }
                    }
                };
                
                $scope.range = function (start, end) {
                    var ret = [];
                    if (!end) {
                        end = start;
                        start = 0;
                    }
                    for (var i = start; i < end; i++) {
                        ret.push(i);
                    }
                    return ret;
                };
                
                $scope.prevPage = function () {
                    if ($scope.currentPage > 0) {
                        $scope.currentPage--;
                    }
                };
                
                $scope.nextPage = function () {
                    if ($scope.currentPage < $scope.pagedItems.length - 1) {
                        $scope.currentPage++;
                    }
                };
                
                $scope.setPage = function () {
                    $scope.currentPage = this.n;
                };

                // functions have been describe process the data for display
                $scope.search();

                // change sorting order
                $scope.sort_by = function(newSortingOrder) {
                    if ($scope.sortingOrder == newSortingOrder)
                        $scope.reverse = !$scope.reverse;

                    $scope.sortingOrder = newSortingOrder;
             
                    // icon setup
                    // $('th i').each(function(){
                    //     // icon reset
                    //     $(this).removeClass().addClass('glyphicon glyphicon-pushpin');
                    // });
                    // if ($scope.reverse)
                    //     $('th.'+ new_sorting_order+' i').removeClass().addClass('glyphicon glyphicon-chevron-up');
                    // else
                    //     $('th.'+ new_sorting_order+' i').removeClass().addClass('glyphicon glyphicon-chevron-down');
                };

    });      

  $scope.deleteimage = function(index, name){

      $http.delete(DbCollection + '/uploader/uploaded/files/' + name).
      success(function(data){
        $scope.innovation.imagedetail.splice(index, 1)[0];
      });

    };

    $rootScope.imagehead = 'Image Actions';
    $scope.image = ['image url', 'image delete', 'image thumbnail'];
    $scope.imageresult = {};

  $scope.onFileSelect = function($files) {
            
        console.log($files);

        //$files: an array of files selected, each file has name, size, and type.
          for (var i = 0; i < $files.length; i++) {

          $scope.file = $files[i];

                $scope.imagedetail = [];
                $scope.imagedetail = $files;
                $scope.innovation.imagedetail = $files;

         document.getElementById('hidden_input').innerHTML += '<input type="text" ng-model="innovation.imagedetail" value="'+ $files[i].name +'" >';
          $scope.upload = $upload.upload({
            url: DbCollection +'/uploader/upload', //upload.php script, node.js route, or servlet url
            method: 'POST', 
            //'POST' or 'PUT',
            //headers: {'header-key': 'header-value'},
            //withCredentials: true,
            data: {myObj: $scope.myModelObj},
            file: $scope.file, // or list of files ($files) for html5 only
            //fileName: 'doc.jpg' or ['1.jpg', '2.jpg', ...] // to modify the name of the file(s)
            // customize file formData name ('Content-Disposition'), server side file variable name. 
            fileFormDataName: $scope.file, //or a list of names for multiple files (html5). Default is 'file' 
            // customize how data is added to formData. See #40#issuecomment-28612000 for sample code
            //formDataAppender: function(formData, key, val){}
          }).progress(function(evt) {

            console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));

          }).success(function(data, status, headers, config) {
            // file is uploaded successfully
            
              
            return $scope.imageresult = data.files;
            // console.log($scope.imageresult.files.length);

            // $scope.image = {};
            // $scope.image = $scope.file;

          });

          //.error(...)
          //.then(success, error, progress); 
          // access or attach event listeners to the underlying XMLHttpRequest.
          //.xhr(function(xhr){xhr.upload.addEventListener(...)})
        }
        /* alternative way of uploading, send the file binary with the file's content-type.
           Could be used to upload files to CouchDB, imgur, etc... html5 FileReader is needed. 
           It could also be used to monitor the progress of a normal http post/put request with large data*/
        // $scope.upload = $upload.http({...})  see 88#issuecomment-31366487 for sample code.
      };



    $scope.toggleMin = function() {
      $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();

    $scope.open = function($event) {
      $event.preventDefault();
      $event.stopPropagation();

      $scope.opened = true;
    };

    $scope.dateOptions = {
      formatYear: 'yy',
      startingDay: 1
    };

    $scope.saveinno = function(){

          return $http.post(DbCollection + '/api/newinnovation/', $scope.innovation)
      		.success(function(data){
              $scope.edituser = data;
              $location.path('/create/'+ $scope.edituser[0].inno_id);
          
          });       

    };

     $scope.editinno = function(id){
            $location.url('/editinno/'+ id);
     };


     $scope.deleteinno = function(index, id){

    if(confirm('are you sure you want to delete id '+ id +'?') == true){
              $http.delete(DbCollection + '/api/newinnovation/' + id).
                success(function(data){
                 // $scope.innovation.splice(index, 1)[0]; 
                window.location.reload();
               });
    }else{
      console.log('canceled');
    };



     };

     $scope.viewinno = function(id){
 
            $location.path('/viewinno/'+ id);
     };

    $rootScope.imagepath = DbCollection + '/uploaded/files/';

});

app.controller('EditInnovationCtrl', function($scope, $alert, $rootScope, $route, $routeParams, innovation, DbCollection, Restangular, $upload, $resource, $http, $q, $filter, ngTableParams, $location){
    
    $scope.models = {
        selected: null,
        templates: [
            {type: "item", id: 2},
            {type: "container", id: 1, columns: [[]]}
        ],
        dropzones: {
            "A": [
                {
                    "type": "item",
                    "id": 7
                },
                {
                    "type": "item",
                    "id": "8"
                },
                {
                    "type": "container",
                    "id": "2",
                    "columns": [
                        [
                            {
                                "type": "item",
                                "id": "9"
                            },
                            {
                                "type": "item",
                                "id": "10"
                            },
                            {
                                "type": "item",
                                "id": "11"
                            }
                        ],
                        [
                            {
                                "type": "item",
                                "id": "12"
                            },
                            {
                                "type": "container",
                                "id": "3",
                                "columns": [
                                    [
                                        {
                                            "type": "item",
                                            "id": "13"
                                        }
                                    ],
                                    [
                                        {
                                            "type": "item",
                                            "id": "14"
                                        }
                                    ]
                                ]
                            },
                            {
                                "type": "item",
                                "id": "15"
                            },
                            {
                                "type": "item",
                                "id": "16"
                            }
                        ]
                    ]
                },
                {
                    "type": "item",
                    "id": 16
                }
            ],
            "B": [
                {
                    "type": "item",
                    "id": 7
                },
                {
                    "type": "item",
                    "id": "8"
                },
                {
                    "type": "container",
                    "id": "2",
                    "columns": [
                        [
                            {
                                "type": "item",
                                "id": "9"
                            },
                            {
                                "type": "item",
                                "id": "10"
                            },
                            {
                                "type": "item",
                                "id": "11"
                            }
                        ],
                        [
                            {
                                "type": "item",
                                "id": "12"
                            },
                            {
                                "type": "container",
                                "id": "3",
                                "columns": [
                                    [
                                        {
                                            "type": "item",
                                            "id": "13"
                                        }
                                    ],
                                    [
                                        {
                                            "type": "item",
                                            "id": "14"
                                        }
                                    ]
                                ]
                            },
                            {
                                "type": "item",
                                "id": "15"
                            },
                            {
                                "type": "item",
                                "id": "16"
                            }
                        ]
                    ]
                },
                {
                    "type": "item",
                    "id": 16
                }
            ],
            "C": [
                {
                    "type": "container",
                    "id": 1,
                    "columns": [
                        [
                            {
                                "type": "item",
                                "id": "1"
                            },
                            {
                                "type": "item",
                                "id": "2"
                            }
                        ],
                        [
                            {
                                "type": "item",
                                "id": "3"
                            }
                        ]
                    ]
                },
                {
                    "type": "item",
                    "id": "4"
                },
                {
                    "type": "item",
                    "id": "5"
                },
                {
                    "type": "item",
                    "id": "6"
                }
            ],
              "D": [
                {
                    "type": "container",
                    "id": 1,
                    "columns": [
                        [
                            {
                                "type": "item",
                                "id": "1"
                            },
                            {
                                "type": "item",
                                "id": "2"
                            }
                        ],
                        [
                            {
                                "type": "item",
                                "id": "3"
                            }
                        ]
                    ]
                },
                {
                    "type": "item",
                    "id": "4"
                },
                {
                    "type": "item",
                    "id": "5"
                },
                {
                    "type": "item",
                    "id": "6"
                }
            ]
        }
    };

    $scope.$watch('models.dropzones', function(model) {
        $scope.modelAsJson = angular.toJson(model, true);
    }, true);

    $scope.innovation = innovation.data;
    
      //     $scope.resourceresult = Innovation;
      // console.log($scope.resourceresult);

      // $scope.innovatioresource = new Innovation({
      //   InnovationName : ['', 'text'],
      //   InnoDescription: ['', 'text'],
      //   email: ['', 'email']
      // });

      $rootScope.imagepath = DbCollection + '/uploaded/files/';

      $http.get(DbCollection + '/api/fields/')
      .then(function(result){
        $scope.datafields = result.data;
      })


      $http.get(DbCollection + '/api/getbricklist').
      success(function(data){
        $scope.bricks = data; 
      });

      $http.get(DbCollection + '/api/fields/column1').
      then(function(result){
        $scope.column1 = result.data; 
        console.log($scope.column1);
      });

      $http.get(DbCollection + '/api/fields/column2').
      then(function(result){
        $scope.column2 = result.data; 
        console.log($scope.column2);
      });

      $http.get(DbCollection + '/api/fields/column3').
      then(function(result){
        $scope.column3 = result.data; 
      });

      $http.get(DbCollection + '/api/fields/column4').
      then(function(result){
        $scope.column4 = result.data; 
      });
// ============== sample function

      $scope.myfunc = function(column){
        var init = column;
        return init;
      };

// ============== end of function
        $rootScope.imagepath = DbCollection + '/uploaded/files/';

        $scope.deleteimage = function(index, name){

          $http.delete(DbCollection + '/uploader/uploaded/files/' + name).
          success(function(data){
            $scope.imagedetail.splice(index, 1)[0];
          });

        }

        $rootScope.imagehead = 'Image Actions';
        $scope.image = ['image url', 'image delete', 'image thumbnail'];
        $scope.imageresult = {};



          $http.get(DbCollection + '/uploader/upload/').
          success(function(data){
            $scope.uploaded = data.files;
          });


          $scope.onFileSelect = function($files) {

            console.log($files);

            //$files: an array of files selected, each file has name, size, and type.
              for (var i = 0; i < $files.length; i++) {

                    $scope.file = $files[i];

                    $scope.imagedetail = [];
                    $scope.imagedetail = $files;
                    
              $scope.upload = $upload.upload({
                url: DbCollection +'/uploader/upload', //upload.php script, node.js route, or servlet url
                method: 'POST', 
                //'POST' or 'PUT',
                //headers: {'header-key': 'header-value'},
                //withCredentials: true,
                data: {myObj: $scope.myModelObj},
                file: $scope.file, // or list of files ($files) for html5 only
                //fileName: 'doc.jpg' or ['1.jpg', '2.jpg', ...] // to modify the name of the file(s)
                // customize file formData name ('Content-Disposition'), server side file variable name. 
                fileFormDataName: $scope.file, //or a list of names for multiple files (html5). Default is 'file' 
                // customize how data is added to formData. See #40#issuecomment-28612000 for sample code
                //formDataAppender: function(formData, key, val){}
              }).progress(function(evt) {

                console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));

              }).success(function(data, status, headers, config) {
                // file is uploaded successfully
                
                  
                return $scope.imageresult = data.files;
                // console.log($scope.imageresult.files.length);

                // $scope.image = {};
                // $scope.image = $scope.file;

              });

              //.error(...)
              //.then(success, error, progress); 
              // access or attach event listeners to the underlying XMLHttpRequest.
              //.xhr(function(xhr){xhr.upload.addEventListener(...)})
            }
            /* alternative way of uploading, send the file binary with the file's content-type.
               Could be used to upload files to CouchDB, imgur, etc... html5 FileReader is needed. 
               It could also be used to monitor the progress of a normal http post/put request with large data*/
            // $scope.upload = $upload.http({...})  see 88#issuecomment-31366487 for sample code.
          };


        $scope.saveinnolast = function(inno_id){
          if ($scope.newInnovation.$invalid){
            $scope.$brodcast('record:invalid');
          }

          $http.post(DbCollection + '/api/saveinno/'+ inno_id, $scope.innovation)
          .success(function(data){
              $scope.result1 = data;
           }); 

          $http.put(DbCollection + '/api/newinnovation/'+ inno_id, $scope.innovation)
          .success(function(data){
              $scope.result2 = data;
              $location.path('/view_inventory');
           });       

        }
      
    $rootScope.imagepath = DbCollection + '/uploaded/files/';


    $scope.result_inno = Restangular.copy($scope.innovation);


   $scope.updateinno = function(id){

      $http.put(DbCollection + '/api/newinnovation/' + id, $scope.innovation)
      .success(function(data){
          $location.path('/view_inventory');
      });
  }

   $scope.deleteimage = function(index, name){

      $http.delete(DbCollection + '/uploader/uploaded/files/' + name).
      success(function(data){
        $scope.innovation.imagedetail.splice(index, 1)[0];
      });

    };

        $rootScope.imagehead = 'Image Actions';
        $scope.image = ['image url', 'image delete', 'image thumbnail'];
        $scope.imageresult = {};


  $scope.onFileSelect = function($files) {
            
        console.log($files);

        //$files: an array of files selected, each file has name, size, and type.
          for (var i = 0; i < $files.length; i++) {

          $scope.file = $files[i];

                $scope.imagedetail = [];
                $scope.imagedetail = $files;
                $scope.innovation.imagedetail = $files;

         // document.getElementById('hidden_input').innerHTML += '<input type="text" ng-model="innovation.imagedetail" value="'+ $files[i].name +'" >';
          $scope.upload = $upload.upload({
            url: DbCollection +'/uploader/upload', //upload.php script, node.js route, or servlet url
            method: 'POST', 
            //'POST' or 'PUT',
            //headers: {'header-key': 'header-value'},
            //withCredentials: true,
            data: {myObj: $scope.myModelObj},
            file: $scope.file, // or list of files ($files) for html5 only
            //fileName: 'doc.jpg' or ['1.jpg', '2.jpg', ...] // to modify the name of the file(s)
            // customize file formData name ('Content-Disposition'), server side file variable name. 
            fileFormDataName: $scope.file, //or a list of names for multiple files (html5). Default is 'file' 
            // customize how data is added to formData. See #40#issuecomment-28612000 for sample code
            //formDataAppender: function(formData, key, val){}
          }).progress(function(evt) {

            console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));

          }).success(function(data, status, headers, config) {
            // file is uploaded successfully
            
              
            return $scope.imageresult = data.files;
            // console.log($scope.imageresult.files.length);

            // $scope.image = {};
            // $scope.image = $scope.file;

          });

          //.error(...)
          //.then(success, error, progress); 
          // access or attach event listeners to the underlying XMLHttpRequest.
          //.xhr(function(xhr){xhr.upload.addEventListener(...)})
        }
        /* alternative way of uploading, send the file binary with the file's content-type.
           Could be used to upload files to CouchDB, imgur, etc... html5 FileReader is needed. 
           It could also be used to monitor the progress of a normal http post/put request with large data*/
        // $scope.upload = $upload.http({...})  see 88#issuecomment-31366487 for sample code.
      };


});

app.controller('getBrickCtrl', function($scope, $alert, $rootScope, getBrick_id, $route, $routeParams, DbCollection, Restangular, $upload, $resource, $http, $q, $filter, ngTableParams, $location){


      $http.get(DbCollection + '/api/fields/')
      .then(function(result){
        $scope.datafields = result.data;
      })

      $scope.innovation = getBrick_id.data[0];

      $http.get(DbCollection + '/api/getbricklist').
      success(function(data){
        console.log(data);
        $scope.bricks = data; 
      });

      $scope.deleteimage = function(index, name){

          $http.delete(DbCollection + '/uploader/uploaded/files/' + name).
          success(function(data){
            $scope.innovation.imagedetail.splice(index, 1)[0];
          });

     };

    $rootScope.imagehead = 'Image Actions';
    $scope.image = ['image url', 'image delete', 'image thumbnail'];
    $scope.imageresult = {};

    $scope.onFileSelect = function($files) {
            
        console.log($files);

        //$files: an array of files selected, each file has name, size, and type.
          for (var i = 0; i < $files.length; i++) {

          $scope.file = $files[i];

                $scope.imagedetail = [];
                $scope.imagedetail = $files;
                $scope.innovation.imagedetail = $files;

         // document.getElementById('hidden_input').innerHTML += '<input type="text" ng-model="innovation.imagedetail" value="'+ $files[i].name +'" >';
          $scope.upload = $upload.upload({
            url: DbCollection +'/uploader/upload', //upload.php script, node.js route, or servlet url
            method: 'POST', 
            //'POST' or 'PUT',
            //headers: {'header-key': 'header-value'},
            //withCredentials: true,
            data: {myObj: $scope.myModelObj},
            file: $scope.file, // or list of files ($files) for html5 only
            //fileName: 'doc.jpg' or ['1.jpg', '2.jpg', ...] // to modify the name of the file(s)
            // customize file formData name ('Content-Disposition'), server side file variable name. 
            fileFormDataName: $scope.file, //or a list of names for multiple files (html5). Default is 'file' 
            // customize how data is added to formData. See #40#issuecomment-28612000 for sample code
            //formDataAppender: function(formData, key, val){}
          }).progress(function(evt) {

            console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));

          }).success(function(data, status, headers, config) {
            // file is uploaded successfully
            
              
            return $scope.imageresult = data.files;
            // console.log($scope.imageresult.files.length);

            // $scope.image = {};
            // $scope.image = $scope.file;

          });

          //.error(...)
          //.then(success, error, progress); 
          // access or attach event listeners to the underlying XMLHttpRequest.
          //.xhr(function(xhr){xhr.upload.addEventListener(...)})
        }
        /* alternative way of uploading, send the file binary with the file's content-type.
           Could be used to upload files to CouchDB, imgur, etc... html5 FileReader is needed. 
           It could also be used to monitor the progress of a normal http post/put request with large data*/
        // $scope.upload = $upload.http({...})  see 88#issuecomment-31366487 for sample code.
      };

});

app.controller('getBricksampleCtrl', function($scope, getBrick_id, $alert, $rootScope, Innovation, $route, $routeParams, DbCollection, Restangular, $upload, $resource, $http, $q, $filter, ngTableParams, $location){


    $scope.models = {
        selected: null,
        templates: [
            {type: "item", id: 2},
            {type: "container", id: 1, columns: [[], []]}
        ],
        dropzones: {
            "A": [
                {
                    "type": "item",
                    "id": 7
                },
                {
                    "type": "item",
                    "id": "8"
                },
                {
                    "type": "container",
                    "id": "2",
                    "columns": [
                        [
                            {
                                "type": "item",
                                "id": "9"
                            },
                            {
                                "type": "item",
                                "id": "10"
                            },
                            {
                                "type": "item",
                                "id": "11"
                            }
                        ],
                        [
                            {
                                "type": "item",
                                "id": "12"
                            },
                            {
                                "type": "container",
                                "id": "3",
                                "columns": [
                                    [
                                        {
                                            "type": "item",
                                            "id": "13"
                                        }
                                    ],
                                    [
                                        {
                                            "type": "item",
                                            "id": "14"
                                        }
                                    ]
                                ]
                            },
                            {
                                "type": "item",
                                "id": "15"
                            },
                            {
                                "type": "item",
                                "id": "16"
                            }
                        ]
                    ]
                },
                {
                    "type": "item",
                    "id": 16
                }
            ],
            "B": [
                {
                    "type": "item",
                    "id": 7
                },
                {
                    "type": "item",
                    "id": "8"
                },
                {
                    "type": "container",
                    "id": "2",
                    "columns": [
                        [
                            {
                                "type": "item",
                                "id": "9"
                            },
                            {
                                "type": "item",
                                "id": "10"
                            },
                            {
                                "type": "item",
                                "id": "11"
                            }
                        ],
                        [
                            {
                                "type": "item",
                                "id": "12"
                            },
                            {
                                "type": "container",
                                "id": "3",
                                "columns": [
                                    [
                                        {
                                            "type": "item",
                                            "id": "13"
                                        }
                                    ],
                                    [
                                        {
                                            "type": "item",
                                            "id": "14"
                                        }
                                    ]
                                ]
                            },
                            {
                                "type": "item",
                                "id": "15"
                            },
                            {
                                "type": "item",
                                "id": "16"
                            }
                        ]
                    ]
                },
                {
                    "type": "item",
                    "id": 16
                }
            ],
            "C": [
                {
                    "type": "container",
                    "id": 1,
                    "columns": [
                        [
                            {
                                "type": "item",
                                "id": "1"
                            },
                            {
                                "type": "item",
                                "id": "2"
                            }
                        ],
                        [
                            {
                                "type": "item",
                                "id": "3"
                            }
                        ]
                    ]
                },
                {
                    "type": "item",
                    "id": "4"
                },
                {
                    "type": "item",
                    "id": "5"
                },
                {
                    "type": "item",
                    "id": "6"
                }
            ],
              "D": [
                {
                    "type": "container",
                    "id": 1,
                    "columns": [
                        [
                            {
                                "type": "item",
                                "id": "1"
                            },
                            {
                                "type": "item",
                                "id": "2"
                            }
                        ],
                        [
                            {
                                "type": "item",
                                "id": "3"
                            }
                        ]
                    ]
                },
                {
                    "type": "item",
                    "id": "4"
                },
                {
                    "type": "item",
                    "id": "5"
                },
                {
                    "type": "item",
                    "id": "6"
                }
            ]
        }
    };

    $scope.$watch('models.dropzones', function(model) {
        $scope.modelAsJson = angular.toJson(model, true);
    }, true);
    
      $scope.innovation = getBrick_id.data[0];

      $scope.resourceresult = Innovation;
      console.log($scope.resourceresult);

      $scope.innovatioresource = new Innovation({
        InnovationName : ['', 'text'],
        InnoDescription: ['', 'text'],
        email: ['', 'email']
      });

      $rootScope.imagepath = DbCollection + '/uploaded/files/';

      $http.get(DbCollection + '/api/fields/')
      .then(function(result){
        $scope.datafields = result.data;
      })


      $http.get(DbCollection + '/api/getbricklist').
      success(function(data){
        $scope.bricks = data; 
      });

      $http.get(DbCollection + '/api/fields/column1').
      then(function(result){
        $scope.column1 = result.data; 
        console.log($scope.column1);
      });

      $http.get(DbCollection + '/api/fields/column2').
      then(function(result){
        $scope.column2 = result.data; 
        console.log($scope.column2);
      });

      $http.get(DbCollection + '/api/fields/column3').
      then(function(result){
        $scope.column3 = result.data; 
      });

      $http.get(DbCollection + '/api/fields/column4').
      then(function(result){
        $scope.column4 = result.data; 
      });
// ============== sample function

      $scope.myfunc = function(column){
        var init = column;
        return init;
      };

// ============== end of function
        $rootScope.imagepath = DbCollection + '/uploaded/files/';

        $scope.deleteimage = function(index, name){

          $http.delete(DbCollection + '/uploader/uploaded/files/' + name).
          success(function(data){
            $scope.imagedetail.splice(index, 1)[0];
          });

        }

        $rootScope.imagehead = 'Image Actions';
        $scope.image = ['image url', 'image delete', 'image thumbnail'];
        $scope.imageresult = {};



          $http.get(DbCollection + '/uploader/upload/').
          success(function(data){
            $scope.uploaded = data.files;
          });


          $scope.onFileSelect = function($files) {

            console.log($files);

            //$files: an array of files selected, each file has name, size, and type.
              for (var i = 0; i < $files.length; i++) {

                    $scope.file = $files[i];

                    $scope.imagedetail = [];
                    $scope.imagedetail = $files;
                    
              $scope.upload = $upload.upload({
                url: DbCollection +'/uploader/upload', //upload.php script, node.js route, or servlet url
                method: 'POST', 
                //'POST' or 'PUT',
                //headers: {'header-key': 'header-value'},
                //withCredentials: true,
                data: {myObj: $scope.myModelObj},
                file: $scope.file, // or list of files ($files) for html5 only
                //fileName: 'doc.jpg' or ['1.jpg', '2.jpg', ...] // to modify the name of the file(s)
                // customize file formData name ('Content-Disposition'), server side file variable name. 
                fileFormDataName: $scope.file, //or a list of names for multiple files (html5). Default is 'file' 
                // customize how data is added to formData. See #40#issuecomment-28612000 for sample code
                //formDataAppender: function(formData, key, val){}
              }).progress(function(evt) {

                console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));

              }).success(function(data, status, headers, config) {
                // file is uploaded successfully
                
                  
                return $scope.imageresult = data.files;
                // console.log($scope.imageresult.files.length);

                // $scope.image = {};
                // $scope.image = $scope.file;

              });

              //.error(...)
              //.then(success, error, progress); 
              // access or attach event listeners to the underlying XMLHttpRequest.
              //.xhr(function(xhr){xhr.upload.addEventListener(...)})
            }
            /* alternative way of uploading, send the file binary with the file's content-type.
               Could be used to upload files to CouchDB, imgur, etc... html5 FileReader is needed. 
               It could also be used to monitor the progress of a normal http post/put request with large data*/
            // $scope.upload = $upload.http({...})  see 88#issuecomment-31366487 for sample code.
          };


        $scope.saveinnolast = function(inno_id){
          if ($scope.newInnovation.$invalid){
            $scope.$brodcast('record:invalid');
          }

          $http.post(DbCollection + '/api/saveinno/'+ inno_id, $scope.innovation)
          .success(function(data){
              $scope.result1 = data;
           }); 

          $http.put(DbCollection + '/api/newinnovation/'+ inno_id, $scope.innovation)
          .success(function(data){
              $scope.result2 = data;
              $location.path('/view_inventory');
           });       

        }
      

});