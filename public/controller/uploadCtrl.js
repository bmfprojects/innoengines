'use strict';

var app = angular.module('innoengines');

app.controller('uploadCtrl', function($scope, $rootScope,  $route, DbCollection, Restangular, $upload, $resource, $http, $q, $filter, ngTableParams, $location){

$rootScope.imagehead = 'Image Actions';
$scope.image = ['image url', 'image delete', 'image thumbnail'];
$scope.imageresult = {};



  $http.get(DbCollection + '/uploader/upload/').
  success(function(data){
    $scope.uploaded = data.files;
  });


  $scope.onFileSelect = function($files) {
  	console.log($files);

    //$files: an array of files selected, each file has name, size, and type.
      for (var i = 0; i < $files.length; i++) {

      $scope.file = $files[i];

      	    $scope.imagedetail = [];
  	        $scope.imagedetail = $files;

      $scope.upload = $upload.upload({
        url: DbCollection +'/uploader/upload', //upload.php script, node.js route, or servlet url
        method: 'POST', 
        //'POST' or 'PUT',
        //headers: {'header-key': 'header-value'},
        //withCredentials: true,
        data: {myObj: $scope.myModelObj},
        file: $scope.file, // or list of files ($files) for html5 only
        //fileName: 'doc.jpg' or ['1.jpg', '2.jpg', ...] // to modify the name of the file(s)
        // customize file formData name ('Content-Disposition'), server side file variable name. 
        fileFormDataName: $scope.file, //or a list of names for multiple files (html5). Default is 'file' 
        // customize how data is added to formData. See #40#issuecomment-28612000 for sample code
        //formDataAppender: function(formData, key, val){}
      }).progress(function(evt) {
        console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
      }).success(function(data, status, headers, config) {
        // file is uploaded successfully
        
          
        return $scope.imageresult = data.files;
        // console.log($scope.imageresult.files.length);

        // $scope.image = {};
        // $scope.image = $scope.file;

      });

      //.error(...)
      //.then(success, error, progress); 
      // access or attach event listeners to the underlying XMLHttpRequest.
      //.xhr(function(xhr){xhr.upload.addEventListener(...)})
    }
    /* alternative way of uploading, send the file binary with the file's content-type.
       Could be used to upload files to CouchDB, imgur, etc... html5 FileReader is needed. 
       It could also be used to monitor the progress of a normal http post/put request with large data*/
    // $scope.upload = $upload.http({...})  see 88#issuecomment-31366487 for sample code.
  };


$scope.uploaditem = function(){

	$http.post(DbCollection + '/uploader/upload/').
	success(function(data){
		console.log(data.files);
	});
}

$scope.deleteimage = function(index, name){

  $http.delete(DbCollection + '/uploader/uploaded/files/' + name).
  success(function(data){
     // $scope.imagedetail.splice($scope.imagedetail.indexOf(name), 1);
    $scope.imagedetail.splice(index, 1)[0];
  });
}


});